# Instancia OR-LIB Problema da Mochila multidimensional: http://people.brunel.ac.uk/~mastjjb/jeb/orlib/mknapinfo.html
import gurobipy as gp
from gurobipy import GRB

import random
from random import seed
from random import randrange

class KnapsackProblem():
    def __init__(self, numItems, dimensions):
        self.numItems = numItems
        self.dimensions = dimensions
        self.p_item = [None] * numItems #lucro de cada item
        self.x_item = [None] * numItems #variáveis: determina se o item está ou não na solução
        self.b_dimension = [None] * dimensions #capacidade máxima de cada instância
        self.w_item_dimension = [None] * numItems #o quanto cada item ocupa de cada dimensão
        for i in range(numItems) :
            self.w_item_dimension[i] = [None] * dimensions
        self.optimalSolution = None # a solução ótima se for conhecida
        seed(256)

    def objectiveFunction(self, variables):
        # print("Calculating objectiveFunction multidimensional knapsack problem..")
        self.x_item = variables

        response = 0
        for i in range(self.numItems) :
            response += (self.p_item[i] * self.x_item[i])

        return response
    def fileName(self, sufix):
        if sufix != None:
            sufix = "-"+sufix
        else:
            sufix = ""

        return str(self.dimensions)+"."+str(self.numItems)+sufix

    def __str__(self):
        return "Knapsack Problem\n n = {}\n m = {}\n optimalSolution = {}\n p_item = {}\n x_item = {}\n b_dimension = {}\n w_item_dimension = {}\n".format(self.numItems, self.dimensions, self.optimalSolution, self.p_item, self.x_item, self.b_dimension, self.w_item_dimension)

class KnapsackORLIBReader():
    def __init__(self, fileName):
        self.fileName = fileName
        self.problems = []

    def defaultRead(self):
        filee = open(self.fileName, 'r')
        qtdTests = filee.readline()

        for test in range(int(qtdTests)):
            filee.readline() #Linha vazia do início
            definition = filee.readline().replace('\n', '').split(" ")[1:] #[num_itens, num_dimensions, optimal_solution]

            if(len(definition) == 2):
                definition[2] = None

            n = int(definition[0])
            m = int(definition[1])
            optimal = float(definition[2])

            currentProblem = KnapsackProblem(n,m)
            currentProblem.optimalSolution = optimal

            itens_profit = filee.readline()
            currentProblem.p_item = itens_profit.replace('\n', '').split(" ")[1:]

            for dim in range(currentProblem.dimensions):
                itemsDimensionWeight = filee.readline() 
                print("itemsDimensionWeight: {}".format(itemsDimensionWeight))
                weightEachItemInDimension = itemsDimensionWeight.replace('\n', '').split(" ")[1:]
                print("weightEachItemInDimension: {}".format(weightEachItemInDimension))
                for item in range(currentProblem.numItems):
                    print("errro aqui - {} {} - no teste {}".format(item, dim, test))
                    currentProblem.w_item_dimension[item][dim] = weightEachItemInDimension[item]

            capacity_dimensions = filee.readline()
            currentProblem.b_dimension = capacity_dimensions.replace('\n', '').split(" ")[1:]
            self.problems.append(currentProblem)
        
        filee.close()
        return self.problems

    def defaultRead2(self, numTests):
        filee = open(self.fileName, 'r')
        lines = filee.readlines()

        linesValues = list(map(lambda line: line.replace('\n', '').strip().split(" "), lines))
        values = []
        for line in linesValues:
            # print("line:{}".format(line))
            if len(line) > 0 and line[0] != '':
                values += line

        qtdTests = values.pop(0)

        if(numTests):
            qtdTests = numTests

        for test in range(int(qtdTests)):
            # print("Test {}".format(test))
            definition = values[0:3] #[num_itens, num_dimensions, optimal_solution]
            # print("definition: {}".format(definition))
            values = values[3:]
            # print("values: {}".format(values))

            if definition[2] == 0:
                definition[2] = None

            n = int(definition[0])
            m = int(definition[1])
            optimal = float(definition[2])

            currentProblem = KnapsackProblem(n,m)
            currentProblem.optimalSolution = optimal

            itens_profit = values[0:n]
            currentProblem.p_item = list(map(lambda x: float(x), itens_profit))
            values = values[n:]

            for dim in range(m):
                for item in range(n):
                    value = values.pop(0)
                    currentProblem.w_item_dimension[item][dim] = float(value)

            capacity_dimensions = values[0:m]
            values = values[m:]
            currentProblem.b_dimension = list(map(lambda x: float(x), capacity_dimensions))

            self.problems.append(currentProblem)
        
        filee.close()
        return self.problems

    def getProblems(self):
        return self.problems

class GurobiSolverKnapsackProblem():
    def __init__(self, problem, limitTime=1800.0, verbose=True):
        self.problem = problem
        self.model = gp.Model("knapsack")
        self.vars = {}
        self.limitTime = limitTime

    def solve(self):
        self.createModel()
        self.optimize()
        self.printSolution()

    def createModel(self):
        for item in range(self.problem.numItems):
            self.vars[item] = self.model.addVar(name="x"+str(item), vtype=GRB.BINARY) #TODO: Melhorar nome das variáveis

        self.model.setObjective(sum(self.vars[i]*self.problem.p_item[i] for i in range(self.problem.numItems)), GRB.MAXIMIZE)
        
        for j in range(self.problem.dimensions):
            self.model.addConstr(sum(self.problem.w_item_dimension[i][j] * self.vars[i] for i in range(self.problem.numItems)) <= self.problem.b_dimension[j], name="limit_dimension")

        self.model.setParam(GRB.Param.TimeLimit, self.limitTime)


    def optimize(self):
        self.model.optimize()

    def printSolution(self):
        if self.model.status == GRB.OPTIMAL:
            print('\nResults')
            print('Cost: {}'.format(self.model.objVal))
            print('Runtime: {}'.format(self.model.Runtime))
            print('Vars:')
            for i in range(self.problem.numItems):
                print('x_{}:{}\n'.format(i,self.vars[i].x))
        else:
            print('No solution')
    
    def getModel(self):
        return self.model
        
class GASolverKnapsackProblem:
    def __init__(self, problem, mutation_rate=0.05, population_size=100,verbose=True):
        self.current_population = []
        self.generations = 100
        self.problem = problem #Passar uma knapsackProblem
        self.verbose = verbose
        self.mutation_rate = mutation_rate
        self.population_size = population_size

    def getBestSol(self):
        return self.bestSol

    def getBestChromosomeSolution(self):
        return self.bestChromosome

    def solve(self, file=None):
        if file != None :
            self.filePrint = True

        self.current_population = self.initializePopulation()
        self.bestChromosome = self.getBestChromosome(self.current_population)
        self.bestSol = self.problem.objectiveFunction(self.bestChromosome)

        print("\n(Gen. 0) BestSol = " + str(self.bestSol))
        if self.filePrint :
            file.write("\n(Gen. 0) BestSol = " + str(self.bestSol))

        for i in range(self.generations) :
            print("\nGeneration {}".format(i))

            parents = self.selectParents(self.current_population)
            offsprings = self.crossover(parents)
            populationGeneration = offsprings + parents
            mutants = self.mutate(populationGeneration)
            newPopulation = self.selectPopulation(mutants)
            self.current_population = newPopulation

            self.bestChromosome = self.getBestChromosome(self.current_population)
            bestSolGeneration = self.problem.objectiveFunction(self.bestChromosome)

            if bestSolGeneration > self.bestSol:
                self.bestSol = bestSolGeneration
                if self.verbose:
                    print("\nGen. " + str(i) + ") BestSol = " + str(self.bestSol))
                    if self.filePrint :
                        file.write("\n(Gen. " + str(i) + ") BestSol = " + str(self.bestSol))

    def initializePopulation(self):
        print("Initalize Population")
        population = []

        for i in range(self.population_size):
            population.append(self.generateRandomChromosome())

        return population

    def generateRandomChromosome(self):
        chromosome = []
        for i in range(self.problem.numItems):
            chromosome.append(randrange(2))

        return chromosome

    def selectParents(self, population):
        print("Select parents")
        parents = []

        for i in range(self.population_size):
            index1 = randrange(self.population_size)
            chromosome1 = self.current_population[index1]

            index2 = randrange(self.population_size)
            chromosome2 = self.current_population[index2]

            if self.problem.objectiveFunction(chromosome1) >= self.problem.objectiveFunction(chromosome2):
                parents.append(chromosome1)
            else:
                parents.append(chromosome2)


        return parents

    def crossover(self, parents):
        print("Crossover")

        population = []
        for i in range(len(parents) - 1):
            new_chromosome_parents = [parents[i], parents[i + 1]]

            child = []
            for geneI in new_chromosome_parents[0]:
                child.append(new_chromosome_parents[randrange(2)][geneI])

            population.append(child)

        return population

    def mutate(self, offsprings):
        print("Mutation")

        for chromosome in offsprings:    # percorre população
            if random.random() < self.mutation_rate:
                total_mutations = randrange(2) + 1  #seleciona total de mutações entre 1 e 2
                for i in range(total_mutations):
                    index = randrange(len(chromosome))
                    chromosome[index] = 1 if chromosome[index] == 0 else 0  # inverte o bit selecionado da solução

        return offsprings

    def selectPopulation(self, population):
        print("Selection")

        populationSorted = population.sort(reverse=True, key=lambda x: self.problem.objectiveFunction(x))
        bestChromosomes = populationSorted[:self.population_size]
        finalPopulation = list(map(lambda chromosome: self.repairOperator(chromosome)))
        return finalPopulation

    def repairOperator(self, chromosome):
        for gene in chromosome:

    def getBestChromosome(self, population):
        print("Get Best Chromosome")

        best_chromosome = population[0]
        best_score = self.problem.objectiveFunction(best_chromosome)

        for i in range(self.population_size):
            current_score = self.problem.objectiveFunction(population[i])
            if current_score > best_score:
                best_score = current_score
                best_chromosome = population[i]

        return best_chromosome


class Main():

    def executeGAPartialTests(self):
        
        tests = [
            ("./instances/mknap1.txt","mknap1",1)
            # ("./instances/mknap1.txt","mknap1",7),
            # ("./instances/mknapcb1.txt","mknapcb1", 1)
        ]

        for (instanceFile, fileName, qtdTests) in tests:
            reader = KnapsackORLIBReader(instanceFile)
            problems = reader.defaultRead2(qtdTests)

            i = 0
            for prob in problems:
                file = open('./results/ga/partial/{}-{}.txt'.format(fileName, prob.fileName(str(i))),'w')
                file.write(str(prob))
                gaSolver = GASolverKnapsackProblem(prob)

                file.write('\n==== Execution ====')
                gaSolver.solve(file)

                bestChromosome = gaSolver.getBestChromosomeSolution()
                bestSol = gaSolver.getBestSol()                    
                file.write('\n==== Results ====')
                file.write('\nCost: '+ str(bestSol))
                # file.write('\nRuntime: '+ str(model.Runtime))
                file.write('\nVars:')
                for id in range(len(bestChromosome)):
                    file.write('\nx_' +str(id)+ ':' + str(bestChromosome[id]))
                
                file.close()
                i+=1

    def executeGurobi(self):
        try:
            tests = [
                ("./instances/mknap1.txt","mknap1",7), 
                ("./instances/mknapcb1.txt","mknapcb1", 5),
                ("./instances/mknapcb2.txt","mknapcb2",5),
                ("./instances/mknapcb3.txt","mknapcb3",5),
                ("./instances/mknapcb4.txt","mknapcb4",5),
                ("./instances/mknapcb5.txt","mknapcb5",5),
                ("./instances/mknapcb6.txt","mknapcb6",5),
                ("./instances/mknapcb7.txt","mknapcb7",5),
                ("./instances/mknapcb8.txt","mknapcb8",5),
                ("./instances/mknapcb9.txt","mknapcb9",5)
            ]

            for (instanceFile, fileName, qtdTests) in tests:
                reader = KnapsackORLIBReader(instanceFile)
                problems = reader.defaultRead2(qtdTests)

                i = 0
                for prob in problems:
                    file = open('./results/lp/{}-{}.txt'.format(fileName, prob.fileName(str(i))),'w')
                    file.write(str(prob))
                    gurobiSolver = GurobiSolverKnapsackProblem(prob)
                    gurobiSolver.solve()
                    model = gurobiSolver.getModel()
                    if model.status == GRB.OPTIMAL:
                        file.write('\n==== Results ====')
                        file.write('\nCost: '+ str(model.objVal))
                        file.write('\nRuntime: '+ str(model.Runtime))
                        file.write('\nVars:')
                        for id in range(prob.numItems):
                            file.write('\nx_' +str(id)+ ':' + str(gurobiSolver.vars[id].x))
                    else:
                        file.write('\n==== Results ====')
                        file.write('\nTime limit reached')
                        file.write('\nCost: '+ str(model.objVal))
                        file.write('\nRuntime: '+ str(model.Runtime))
                        file.write('\nVars:')
                        for id in range(prob.numItems):
                            file.write('\nx_' +str(id)+ ':' + str(gurobiSolver.vars[id].x))
                    i+=1
                    file.close()

        except gp.GurobiError as e:
            print('Error code ' + str(e.errno) + ': ' + str(e))
            file.close()

        except AttributeError as e:
            print('Encountered an attribute error')
            print('Stack', e)
            file.close()

        except Exception as e:        
            print('Stack', e)
            file.close()


main = Main()
main.executeGAPartialTests()